import { Command } from 'commander';

export type CommandArguments = Record<string, string>;
export type CommandOptions = Record<string, string>;

export interface CommandInterface {
  getIdentifier (): string;
  getParentCommand (): string | null;
  configureCommand (command: Command): void;
  executeCommand (args: CommandArguments, options: CommandOptions, command: Command): Promise<number | null>;
}
