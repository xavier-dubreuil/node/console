import { Command, ParseOptions } from 'commander';
import { CommandInterface } from './command';

export const ConsoleServiceSymbol = Symbol.for('ConsoleService');

export const UNKOWN = 'Unkonwn Command';

export class UsageError extends Error { }
export class UnkownCommandError extends Error { }

type ActionArguments = (string | Record<string, string> | Command)[];

export class Console {
  private program: Command;

  constructor (
    private readonly commands: CommandInterface[],
    private readonly name: string,
    private readonly description: string,
    private readonly version: string
  ) {
    this.program = new Command();
    this.initialize();
  }

  private getCommands (parent: string | null): CommandInterface[] {
    return this.commands.filter(item => item.getParentCommand() === parent);
  }

  private initialize (): void {
    this.program
      .name(this.name)
      .description(this.description)
      .version(this.version);
    this.initializeCommands(this.program, this.getCommands(null));
  }

  private initializeCommands (program: Command, commands: CommandInterface[]): void {
    for (const command of commands) {
      const cmd = program.command(command.getIdentifier());
      command.configureCommand(cmd);
      const subCommands = this.getCommands(command.getIdentifier());
      if (subCommands.length === 0) {
        cmd
          .action(async (...args: ActionArguments) => await this.action(command, ...args))
      } else {
        cmd.configureHelp({
          sortSubcommands: true,
          showGlobalOptions: false,
          sortOptions: true,
        });
        this.initializeCommands(cmd, subCommands);
      }
    }
  }

  protected async action (command: CommandInterface, ...args: ActionArguments): Promise<void> {
    const cmd: Command = args.pop() as Command;
    const options = cmd.opts();
    const argv: Record<string, string> = cmd.registeredArguments.reduce((acc, item, index) => {
      return { ...acc, [item.name()]: cmd.args[index] };
    }, {});
    await command.executeCommand(argv, options, cmd);
  }

  public async execute (args?: string[], options?: ParseOptions): Promise<void> {
    await this.program.parseAsync(args, options);
  }
}
