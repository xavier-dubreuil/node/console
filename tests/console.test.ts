import { jest, expect, describe, it } from '@jest/globals';
import { CommandArguments, CommandInterface, CommandOptions, Console } from '../src';
import { Command } from 'commander';

describe('Console', () => {
  it('Nominal', async () => {
    class TestCommand implements CommandInterface {
      constructor (private readonly identifier: string, private readonly parent: string | null = null) { }
      getIdentifier (): string { return this.identifier }
      getParentCommand (): string | null { return this.parent }
      configureCommand (command: Command): void {
        if (this.parent !== null) {
          command.argument('<name>').option('-o, --opt <opt>');
        }
      }

      public async executeCommand (args: CommandArguments, options: CommandOptions): Promise<number> {
        expect(args).toEqual({ 'name': 'arg-name' });
        expect(options).toEqual({ 'opt': 'my-opt' });
        return 0;
      }
    }

    const parent = new TestCommand('parent');
    const command = new TestCommand('test', 'parent');
    const consoleInstance = new Console([parent, command], 'test', 'Description', '0.1.0');

    const exit = jest.spyOn(process, 'exit');
    exit.mockImplementation(() => undefined as never);

    await consoleInstance.execute(['node', 'test.js', 'parent', 'test', 'arg-name', '--opt', 'my-opt']);
  });
});
